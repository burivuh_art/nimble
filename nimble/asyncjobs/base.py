from multiprocessing import Process, Pipe
from datetime import datetime
import hashlib, Queue, inspect, functools, os, os.path
import time, traceback, json
from nimble.tools import get_read_dir

read_dir = get_read_dir() #low-level reading N items from the directory (useful for large directories)

class Job(object):
    def __init__(self, command, args=None, kwargs=None, jid=None):
        self.command = command
        self.args = args or []
        self.kwargs = kwargs or {}
        self.jid = jid or '%s_%s_%s'%(time.time(), hash(tuple(args)), hash(tuple(kwargs.items())))

    def __str__(self):
        return json.dumps([self.command, self.args, self.kwargs])

    @staticmethod
    def loads(name, text):
        command, args, kwargs = json.loads(text)
        return Job(jid=name, command=command, args=args, kwargs=kwargs)

class FunctionProxyProcessor(object):
    def __init__(self, proc):
        self.proc = proc

    def pre_multiprocess(self):
        getattr(self.proc, 'pre_multiprocess', lambda:None)()

    def process(self, job):
        return getattr(self.proc, job.command)(*job.args, **job.kwargs)

class Dispatcher(object):
    worker_dir='workers/'

    def __init__(self, proc, log_dir, multiprocess=16):
        dtnow = datetime.now()
        wid = '%s_worker'%dtnow

        self.multiprocess = multiprocess

        self.log_dir = log_dir
        self.worker_dir = os.path.join(self.log_dir, Dispatcher.worker_dir)
        self.dispatcher_log_file = os.path.join(self.log_dir, 'jobs.log')
        Dispatcher.worker_log_file = os.path.join(self.log_dir, '%s.log'%wid)

        try:
            os.mkdir(self.log_dir)
        except OSError:
            pass

        try:
            os.mkdir(self.worker_dir)
        except OSError:
            pass

        with open(self.dispatcher_log_file, 'a') as log:
            pass

        with open(self.worker_log_file, 'a') as log:
            pass

        self.log('start', dtnow)

        active_read, self.active = Pipe(duplex=False)

        self.worker = Process(target=Dispatcher._worker_proc, args=(wid, self.worker_dir, proc, active_read, self.multiprocess))

        self.worker.start()

        self.log('worker', data=[self.worker.pid, wid])

    def log(self, action, data=None):
        procs = {
            'drop': lambda job: '<dropped_job id="%s" command="%s" args="%s" time="%s" />\n'%(job.jid, job.command, job.args, datetime.now()),
            'start': lambda dtnow: '<log start="%s">\n'%dtnow,
            'dispatched': lambda data: 'dispatched %s\n'%data,
            'close': lambda x: '</log>\n',
            'worker': lambda job: '<worker pid="%s" wid="%s" time="%s" />\n'%(data[0], data[1], datetime.now()),
            'error': lambda ex: '<error time="%s">%s</error>\n'%(datetime.now(), ex),
        }
        with open(self.dispatcher_log_file, 'a') as log:
            log.write(procs[action](data))

    @staticmethod
    def worker_log(worker, action, job=None):
        procs = {
            'start': lambda job: 'log_started %s %s\n'%(datetime.now(), worker),
            'close': lambda job: 'log_closed %s\n'%worker,
            'process_error': lambda ex: 'processing_error %s %s\n'%(datetime.now(), traceback.format_exc()),
            'load_error': lambda ex: 'loading_error %s %s\n'%(datetime.now(), traceback.format_exc()),
        }
        with open(Dispatcher.worker_log_file, 'a') as log:
            log.write(procs[action](job))

    @staticmethod
    def _worker_proc(wid, worker_dir, proc, active, multiprocess):
        Dispatcher.worker_log(wid, 'start', None)

        def process(jdir, jfile, proc):
            fname = os.path.join(jdir, jfile)
            with open(fname, 'r') as jf:
                try:
                    data = jf.read()
                    job = Job.loads(jfile, data)
                except Exception, e:
                    Dispatcher.worker_log(wid, 'load_error', '%s: %s'%(e, data))
                    return False

                try:
                    r = proc.process(job)
                    if not r:
                        raise Exception('proc return False for: %s'%job)
                except Exception as e:
                    Dispatcher.worker_log(wid, 'process_error', e)
                    return False
            os.remove(fname)
            return True

        max_tasks = multiprocess if multiprocess > 0 else 32

        while True:
            try:
                if active.poll():
                    raise EOFError()

                fnames = read_dir(worker_dir, max_items=max_tasks)
                while fnames:
                    if len(fnames) > 2 and multiprocess > 0:
                        proc.pre_multiprocess()
                        procs = [Process(target=process, args=(worker_dir, fn, proc)) for fn in fnames]
                        [p.start() for p in procs]
                        [p.join() for p in procs]
                    else:
                        for fn in fnames:
                            process(worker_dir, fn, proc)

                    if active.poll():
                        raise EOFError()
                    fnames = read_dir(worker_dir, max_items=max_tasks)

                time.sleep(0.1)
            except EOFError:
                break
            except:
                Dispatcher.worker_log(wid, 'process_error', traceback.format_exc())
                break

        Dispatcher.worker_log(wid, 'close', None)

    def dispatch(self, command, args, kwargs):
        job = Job(command=command, args=args, kwargs=kwargs)
        try:
            fname = os.path.join(self.worker_dir, job.jid)
            with open(fname, 'w') as jf:
                jf.write(str(job))
            #self.log('dispatched', '%s: %s'%(fname, job))
            return True
        except:
            self.log('error', traceback.format_exc())
            return False

    def shutdown(self):
        self.active.send(0)
        self.active.close()
        self.worker.join()
        self.log('close')

class AsyncJobManager(object):
    def __init__(self, proc, log_dir, multiprocess=16):
        self.proc = FunctionProxyProcessor(proc)
        self.dispatcher = Dispatcher(proc=self.proc,
                                     log_dir=log_dir,
                                     multiprocess=multiprocess)

        for name, meth in inspect.getmembers(proc, inspect.ismethod):
            setattr(self, meth.__name__, AsyncJobManager.wrapped_dispatch(meth, self.dispatcher))

    def shutdown(self):
        self.dispatcher.shutdown()

    @staticmethod
    def wrapped_dispatch(func, dispatcher):
        @functools.wraps(func)
        def dispatchable(*args, **kwargs):
            return dispatcher.dispatch(command=func.__name__, args=args, kwargs=kwargs)
        return dispatchable
