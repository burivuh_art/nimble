import sys
import socket
import datetime, cStringIO, functools
from nimble.protocols.tools import make_client_connection, DEFAULT_PROTOCOL
from nimble.errors import NimbleException

def request_over_http(self, shared_name, shared_method):
    @functools.wraps(shared_method)
    def f(*args, **kwargs):
        connection = make_client_connection(self.server, protocol=self.protocol,
                                            secret=self.secret)
        return connection.request(data=(shared_name, args, kwargs))
    return f

def request_over_direct_call(self, shared_name, shared_method):
    def direct_call(server, data, path_info):
        environ = {'REMOTE_ADDR': '0.0.0.0'}
        environ['PATH_INFO'] = path_info
        environ['CONTENT_LENGTH'] = 2*len(data)-1
        environ['wsgi.input'] = cStringIO.StringIO(data)

        res = server.process_request(lambda x,y:x, environ)
        return res
    
    @functools.wraps(shared_method)
    def f(*args, **kwargs):
        connection = make_client_connection(server=None)
        postBody = connection.dump_request(data=(shared_name, args, kwargs))
        response = direct_call(self.serverobj, postBody, connection.server)
        isError, answer = connection.load_response(response)
        if isError:
            raise NimbleException.load(answer)
        return answer
    return f

class ServerClient(object):
    """
    base class to be used as superclass for 'RPC' client to every nimble server
    parses server object using SERVER_CLASS field to bind the same name wrapper methods to his instance object; such wrappers serialize wrapped function signature to nimble protocol, make sync request over the net and deserialize answer
    """
    SERVER_CLASS = None

    def __init__(self, server, log=None, default_protocol=DEFAULT_PROTOCOL, request_maker=request_over_http, secret=None):
        self.serverobj = self.SERVER_CLASS(log=log)
        self.protocol = default_protocol
        self.server = server
        self.secret = secret

        for nv, meth in self.serverobj._callbacks.items():
            f = request_maker(self, shared_name=nv, shared_method=meth)
            setattr(self, meth.__name__, f)

class DirectClient(object):
    def __init__(self, server_instance):
        self.serverobj = server_instance

        for nv, meth in self.serverobj._callbacks.items():
            setattr(self, meth.__name__, request_over_direct_call(self, shared_name=nv, shared_method=meth))

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.serverobj.shutdown()

class StandaloneClient(object):
     def __init__(self, server, default_protocol=DEFAULT_PROTOCOL, request_maker=request_over_http, secret=None):
        self.protocol = default_protocol
        self.server = server
        self.secret = secret
   
        self.saved_init_proc = functools.partial(self.init_proc,
                                                 default_protocol=default_protocol,
                                                 request_maker=request_maker,
                                                 secret=secret
        )
        self.initialized = False

     def init_proc(self, default_protocol, request_maker, secret):
        empty = lambda: None
        f = request_maker(self, shared_name="", shared_method=empty)
        signatures = f()

        for meth_name in signatures:
            f = request_maker(self, shared_name=meth_name, shared_method=empty)
            setattr(self, meth_name, f)

     def __getattribute__(self, name):
        if name not in ('__init__', 'initialized', 'saved_init_proc', 'init_proc',
                         'server', 'protocol', 'secret') and\
           not self.initialized:
            self.saved_init_proc()
            self.initialized = True
        return object.__getattribute__(self, name)
