import datetime, cStringIO, functools, operator

from nimble.errors import NimbleException
from nimble.tools import object_type_string
from nimble.client.base import ServerClient, request_over_direct_call
from nimble.protocols.tools import make_client_connection

NOW = datetime.datetime.now

#test no error raising
def test(func):
    func_name = func.__name__
    @functools.wraps(func)
    def f(*args, **kwargs):
        try:
            res, t, data = func(*args, **kwargs)
            print _ok_str(func_name, t, data, res)
            return res
        except NimbleException, ex:
            print _err_str(func_name, 'Server return error: %s'%ex)
    return f

#test on error raising
def test_fail(func):
    func_name = func.__name__
    @functools.wraps(func)
    def f(*args, **kwargs):
        try:
            res, t, data = func(*args, **kwargs)
            print _err_str(func_name, res, True)
        except NimbleException, ex:
            print _ok_str(func_name, None, None, object_type_string(ex), True)
    return f

#test comparing the result with params
def expect_result(expected_result):
    def decorator(func):
        func_name = func.__name__
        @functools.wraps(func)
        def f(*args, **kwargs):
            try:
                res, t, data = func(*args, **kwargs)
                success = res == expected_result
                if callable(expected_result):
                    success = expected_result(res)
                if success:
                    print _ok_str(func_name, t, data, res)
                    return res
                else:
                    print _err_str(func_name, '%s != %s'%(expected_result, res))
            except NimbleException, ex:
                print _err_str(func_name, 'Server return error: %s'%ex)
        return f
    return decorator

   
def _err_str(func_name, res, fail=False, fullInfo=True):
    scheme = 'ERROR: %s'
    params = [func_name]
    if fail:
        scheme = 'ERROR (NO FAIL): %s'
    if fullInfo:
        scheme += ': %s'
        params.append(str(res)[:1000]+'...')
    return scheme%tuple(params)

def _ok_str(func_name, t, data, res, fail=False, fullInfo=True):
    scheme = 'SUCCESS (%s): %s'
    params = [t, func_name]
    if fail:
        scheme = 'SUCCESS ON FAIL (%s): %s'
    if fullInfo:
        scheme += ': %s'
        params.append(str(res)[:32]+'...')
    return scheme%tuple(params)

class TestLogger(object):
    def debug(self, msg):
        print 'SERVER LOG: %s'%msg

class TestingClient(ServerClient):
    def __init__(self, serverClass):
        self.SERVER_CLASS = serverClass

        print 'rescode (overheaded time): name: additional info'
        print '--------------------------------------------'
        connection = make_client_connection(server=None)
        print 'server is tested with connection: %s'%object_type_string(connection)
        print '--------------------------------------------'

        ServerClient.__init__(self, server=None, log=TestLogger(), request_maker=request_over_direct_call)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.serverobj.shutdown()
