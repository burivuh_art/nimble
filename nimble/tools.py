import sys
import os
import logging
import inspect

import ctypes

def get_read_dir():
    
    if not sys.platform.startswith('linux'):
        raise Exception("read_dir works only for linux-based systems. Sorry")
    
    class DIRENT(ctypes.Structure):
            _fields_ = [
            ('d_ino', ctypes.c_long),                 # inode number
            ('d_off', ctypes.c_long),                 # offset to the next dirent
            ('d_reclen', ctypes.c_ushort),    # length of this record
            ('d_type', ctypes.c_ubyte),       # type of file; not supported
                                              #   by all file system types
            ('d_name', ctypes.c_char * 256),  # filename
            ]
    
    C = ctypes.CDLL(None)
    readdir_r = C.readdir_r
    readdir_r.argtypes = [ctypes.c_void_p, ctypes.POINTER(DIRENT), ctypes.POINTER(ctypes.POINTER(DIRENT))]
    readdir_r.restype = ctypes.c_int

    fdopendir = C.fdopendir
    fdopendir.argtypes = [ctypes.c_int]
    fdopendir.restype = ctypes.c_void_p

    closedir = C.closedir
    closedir.argtypes = [ctypes.c_void_p]
    closedir.restype = ctypes.c_int

    def read_dir(dirname, max_items=32):
        dirent = DIRENT()
        result = ctypes.POINTER(DIRENT)()
        fnames = []
        dfd = os.open(dirname, os.O_RDONLY)
        dir_stream = fdopendir(dfd)
        try:
            while len(fnames) < max_items:
                code = readdir_r(dir_stream, dirent, result)
                if code:
                    break#error
                if not result:
                    break
                if not dirent.d_name.startswith('.'):
                    fnames.append(dirent.d_name)
            return fnames
        finally:
            closedir(dir_stream)
    return read_dir

def setup_django_environment(settings_module_string):
    import os
    os.environ['DJANGO_SETTINGS_MODULE'] = settings_module_string

class LazyProperty(object):
    def __init__(self, getter):
        self.data = {}
        self.getter = getter

    def __get__(self, instance, owner):
        if instance not in self.data:
            self.data[instance] = self.getter(instance, owner)
        return self.data[instance]

    def __delete__(self, instance):
        try:
            del self.data[instance]
        except KeyError, ex:
            pass

def object_type(obj):
    cls = type(obj)
    return inspect.getmodule(cls).__name__, cls.__name__

def object_type_string(obj):
    return '.'.join(object_type(obj))


def die(msg):
    print >> sys.stderr, msg
    sys.exit(2)

class Log(object):
    def __init__(self, dir, log, level=logging.DEBUG):
        self.log = logging.getLogger()
        self.log.setLevel(level)

        file = os.path.join(dir, log)
        try:
            handler = logging.FileHandler(file, 'a', 'utf-8')
        except IOError as e:
            die("Can't open log file %s\n%s" % (file, e.strerror))

        formatter = logging.Formatter("%(asctime)s %(levelname)s %(module)s %(message)s", "%m-%d %H:%M:%S")
        handler.setFormatter(formatter)
        self.log.addHandler(handler)

